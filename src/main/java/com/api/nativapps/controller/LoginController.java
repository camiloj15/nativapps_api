package com.api.nativapps.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.api.nativapps.model.UserCredential;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginController.
 */
@Controller
public class LoginController {
	
	
	/** The Constant LOG. */
	public final static Log LOG = LogFactory.getLog(LoginController.class);


	@PostMapping("/login")
	public ResponseEntity<Object> loginCheck(@RequestBody UserCredential userCredential){
		if(userCredential.getUsername().equals("user") && userCredential.getPassword().equals("password")){
//			String hashed = BCrypt.hashpw(userCredential.getUsername()+userCredential.getPassword(), BCrypt.sal());
			return new ResponseEntity<Object>("Ok", HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>("usuario o contraseña invalida", HttpStatus.OK);
		}
	}
}
