package com.api.nativapps.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.nativapps.constants.UtilsConstants;
import com.api.nativapps.model.CourseModel;
import com.api.nativapps.service.CourseService;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseController.
 */
@RestController
@RequestMapping("/course")
public class CourseController {

	/** The Constant LOG. */
	private static final Log LOG = LogFactory.getLog(CourseController.class);
	
	/** The course service. */
	@Autowired
	@Qualifier("courseServiceImpl")
	private CourseService courseService;
	
	/**
	 * Adds the.
	 *
	 * @param courseModel the course model
	 * @return the response entity
	 */
	@PostMapping("")
	public ResponseEntity<CourseModel> add(@RequestBody CourseModel courseModel){
		LOG.info("Method: add() -- PARAMS:"+ courseModel.toString());
		CourseModel course = courseService.add(courseModel);
		return new ResponseEntity<CourseModel>(course, HttpStatus.CREATED);
	}
	
	/**
	 * Update.
	 *
	 * @param courseModel the course model
	 * @return the response entity
	 */
	@PutMapping("")
	public ResponseEntity<Object> update(@RequestBody CourseModel courseModel){
		LOG.info("Method: update() -- PARAMS:"+ courseModel.toString());
		CourseModel course = courseService.findModelByCode(courseModel.getCode());
		if(course != null){
			CourseModel course_updated = courseService.add(courseModel);
			return new ResponseEntity<Object>(course_updated, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.COURSE_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete.
	 *
	 * @param code the code
	 * @return the response entity
	 */
	@DeleteMapping("")
	public ResponseEntity<String> delete(@RequestParam(name="code", required=true) String code){
		LOG.info("Method: delete() -- PARAMS: id="+ code);
		try {
			courseService.remove(code);
			return new ResponseEntity<String>(UtilsConstants.COURSE_DELETED, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(UtilsConstants.COURSE_ERROR_DELETED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * List all.
	 *
	 * @return the response entity
	 */
	@GetMapping("")
	public ResponseEntity<List<CourseModel>> listAll(){
		LOG.info("Method: listAll() ");
		courseService.listAll();
		return new ResponseEntity<List<CourseModel>>(new ArrayList<>(courseService.listAll()), HttpStatus.OK);
	}

}
