package com.api.nativapps.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.nativapps.constants.UtilsConstants;
import com.api.nativapps.model.TeacherModel;
import com.api.nativapps.service.TeacherService;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherController.
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {

/** The Constant LOG. */
private static final Log LOG = LogFactory.getLog(TeacherController.class);
	
	/** The teacher service. */
	@Autowired
	@Qualifier("teacherServiceImpl")
	private TeacherService teacherService;

	/**
	 * Adds the.
	 *
	 * @param teacherModel the teacher model
	 * @return the response entity
	 */
	@PostMapping("")
	public ResponseEntity<TeacherModel> add(@RequestBody TeacherModel teacherModel){
		LOG.info("Method: add() -- PARAMS:"+ teacherModel.toString());
		TeacherModel student = teacherService.add(teacherModel);
		return new ResponseEntity<TeacherModel>(student, HttpStatus.CREATED);
	}
	
	/**
	 * Update.
	 *
	 * @param teacherModel the teacher model
	 * @return the response entity
	 */
	@PutMapping("")
	public ResponseEntity<Object> update(@RequestBody TeacherModel teacherModel){
		LOG.info("Method: update() -- PARAMS:"+ teacherModel.toString());
		TeacherModel student = teacherService.findModelById(teacherModel.getId());
		if(student != null){
			TeacherModel update_student = teacherService.add(teacherModel);
			return new ResponseEntity<Object>(update_student, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.STUDENT_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@DeleteMapping("")
	public ResponseEntity<String> delete(@RequestParam(name="id", required=true) int id){
		LOG.info("Method: delete() -- PARAMS: id="+ id);
		try{
		teacherService.remove(id);
		return new ResponseEntity<String>(UtilsConstants.STUDENT_DELETED, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(UtilsConstants.STUDENT_ERROR_DELETED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * List all.
	 *
	 * @return the response entity
	 */
	@GetMapping("")
	public ResponseEntity<List<TeacherModel>> listAll(){
		LOG.info("Method: listAll() ");
		teacherService.listAll();
		return new ResponseEntity<List<TeacherModel>>(new ArrayList<>(teacherService.listAll()), HttpStatus.OK);
	}

}
