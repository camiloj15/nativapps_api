package com.api.nativapps.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.nativapps.constants.UtilsConstants;
import com.api.nativapps.model.StudentModel;
import com.api.nativapps.service.StudentService;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentController.
 */
@RestController
@RequestMapping("/student")
public class StudentController {
	
	/** The Constant LOG. */
	private static final Log LOG = LogFactory.getLog(StudentController.class);
	
	/** The student service. */
	@Autowired
	@Qualifier("studentServiceImpl")
	private StudentService studentService;

	/**
	 * Adds the.
	 *
	 * @param studentModel the student model
	 * @return the response entity
	 */
	@PostMapping("")
	public ResponseEntity<StudentModel> add(@RequestBody StudentModel studentModel){
		LOG.info("Method: add() -- PARAMS:"+ studentModel.toString());
		StudentModel student = studentService.add(studentModel);
		return new ResponseEntity<StudentModel>(student, HttpStatus.CREATED);
	}
	
	/**
	 * Update.
	 *
	 * @param studentModel the student model
	 * @return the response entity
	 */
	@PutMapping("")
	public ResponseEntity<Object> update(@RequestBody StudentModel studentModel){
		LOG.info("Method: update() -- PARAMS:"+ studentModel.toString());
		StudentModel student = studentService.findModelById(studentModel.getId());
		if(student != null){
			StudentModel update_student = studentService.add(studentModel);
			return new ResponseEntity<Object>(update_student, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.TEACHER_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@DeleteMapping("")
	public ResponseEntity<String> delete(@RequestParam(name="id", required=true) int id){
		LOG.info("Method: delete() -- PARAMS: id="+ id);
		try{
			studentService.remove(id);
		return new ResponseEntity<String>(UtilsConstants.TEACHER_DELETED, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(UtilsConstants.TEACHER_ERROR_DELETED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * List all.
	 *
	 * @return the response entity
	 */
	@GetMapping("")
	public ResponseEntity<List<StudentModel>> listAll(){
		LOG.info("Method: listAll() ");
		studentService.listAll();
		return new ResponseEntity<List<StudentModel>>(new ArrayList<>(studentService.listAll()), HttpStatus.OK);
	}

}
