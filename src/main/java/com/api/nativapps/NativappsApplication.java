package com.api.nativapps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NativappsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NativappsApplication.class, args);
	}
}
