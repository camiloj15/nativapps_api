package com.api.nativapps.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.api.nativapps.entity.Student;
import com.api.nativapps.model.StudentModel;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentConverter.
 */
@Component("studentConverter")
public class StudentConverter {
	
	/**
	 * Convert model to entity.
	 *
	 * @param studentModel the student model
	 * @return the student
	 */
	public Student convertModelToEntity(StudentModel studentModel){
		Student student = new Student();
		student.setGenre(studentModel.getGenre());
		student.setId(studentModel.getId());
		student.setIdentification(studentModel.getIdentification());
		student.setLastName(studentModel.getLastName());
		student.setName(studentModel.getName());
		return student;
	}

	/**
	 * Convert entity to model.
	 *
	 * @param student the student
	 * @return the student model
	 */
	public StudentModel convertEntityToModel(Student student){
		StudentModel studentModel = new StudentModel();
		studentModel.setGenre(student.getGenre());
		studentModel.setId(student.getId());
		studentModel.setIdentification(student.getIdentification());
		studentModel.setLastName(student.getLastName());
		studentModel.setName(student.getName());
		return studentModel;	
	}
	
	/**
	 * Convert model list to entity list.
	 *
	 * @param studentModel the student model
	 * @return the list
	 */
	public List<Student> convertModelListToEntityList(List<StudentModel> studentModel){
		List<Student> students = new ArrayList<Student>();
		for(StudentModel model: studentModel){
			students.add(convertModelToEntity(model));
		}
		return students;
	}
	
	/**
	 * Convert entity list to model list.
	 *
	 * @param students the students
	 * @return the list
	 */
	public List<StudentModel> convertEntityListToModelList(List<Student> students){
		List<StudentModel> studentsModel = new ArrayList<StudentModel>();
		for(Student entity: students){
			studentsModel.add(convertEntityToModel(entity));
		}
		return studentsModel;
	}


}
