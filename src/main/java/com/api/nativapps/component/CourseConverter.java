package com.api.nativapps.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.api.nativapps.entity.Course;
import com.api.nativapps.entity.Teacher;
import com.api.nativapps.model.CourseModel;
import com.api.nativapps.model.TeacherModel;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseConverter.
 */
@Component("courseConverter")
public class CourseConverter {

	/**
	 * Convert model to entity.
	 *
	 * @param courseModel the course model
	 * @return the course
	 */
	public Course convertModelToEntity(CourseModel courseModel){
		Course course = new Course();
		course.setCode(courseModel.getCode());
		course.setObservation(courseModel.getObservation());
		course.setStudents_course(courseModel.getStudents_course());
		course.setTeacher(courseModel.getTeacher());
		course.setName(courseModel.getName());
		return course;
	}

	/**
	 * Convert entity to model.
	 *
	 * @param course the course
	 * @return the course model
	 */
	public CourseModel convertEntityToModel(Course course){
		CourseModel courseModel = new CourseModel();
		courseModel.setCode(course.getCode());
		courseModel.setObservation(course.getObservation());
		courseModel.setStudents_course(course.getStudents_course());
		courseModel.setTeacher(course.getTeacher());
		courseModel.setName(course.getName());
		return courseModel;
	}
	
	/**
	 * Convert model list to entity list.
	 *
	 * @param courseModel the course model
	 * @return the list
	 */
	public List<Course> convertModelListToEntityList(List<CourseModel> courseModel){
		List<Course> courses = new ArrayList<Course>();
		for(CourseModel model: courseModel){
			courses.add(convertModelToEntity(model));
		}
		return courses;
	}
	
	/**
	 * Convert entity list to model list.
	 *
	 * @param courses the courses
	 * @return the list
	 */
	public List<CourseModel> convertEntityListToModelList(List<Course> courses){
		List<CourseModel> courseModels = new ArrayList<CourseModel>();
		for(Course entity: courses){
			courseModels.add(convertEntityToModel(entity));
		}
		return courseModels;
	}
}
