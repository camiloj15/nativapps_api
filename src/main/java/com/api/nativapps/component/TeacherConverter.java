package com.api.nativapps.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.api.nativapps.entity.Teacher;
import com.api.nativapps.model.TeacherModel;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherConverter.
 */
@Component("teacherConverter")
public class TeacherConverter {
	
	/**
	 * Convert model to entity.
	 *
	 * @param teacherModel the teacher model
	 * @return the teacher
	 */
	public Teacher convertModelToEntity(TeacherModel teacherModel){
		Teacher teacher = new Teacher();
		teacher.setGenre(teacherModel.getGenre());
		teacher.setId(teacherModel.getId());
		teacher.setIdentification(teacherModel.getIdentification());
		teacher.setLastName(teacherModel.getLastName());
		teacher.setName(teacherModel.getName());
		return teacher;
	}

	/**
	 * Convert entity to model.
	 *
	 * @param teacher the teacher
	 * @return the teacher model
	 */
	public TeacherModel convertEntityToModel(Teacher teacher){
		TeacherModel teacherModel = new TeacherModel();
		teacherModel.setGenre(teacher.getGenre());
		teacherModel.setId(teacher.getId());
		teacherModel.setIdentification(teacher.getIdentification());
		teacherModel.setLastName(teacher.getLastName());
		teacherModel.setName(teacher.getName());
		return teacherModel;	
	}
	
	/**
	 * Convert model list to entity list.
	 *
	 * @param teacherModel the teacher model
	 * @return the list
	 */
	public List<Teacher> convertModelListToEntityList(List<TeacherModel> teacherModel){
		List<Teacher> teachers = new ArrayList<Teacher>();
		for(TeacherModel model: teacherModel){
			teachers.add(convertModelToEntity(model));
		}
		return teachers;
	}
	
	/**
	 * Convert entity list to model list.
	 *
	 * @param teachers the teachers
	 * @return the list
	 */
	public List<TeacherModel> convertEntityListToModelList(List<Teacher> teachers){
		List<TeacherModel> teachersModel = new ArrayList<TeacherModel>();
		for(Teacher entity: teachers){
			teachersModel.add(convertEntityToModel(entity));
		}
		return teachersModel;
	}


}
