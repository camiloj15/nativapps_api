package com.api.nativapps.service;

import java.util.List;

import com.api.nativapps.entity.Student;
import com.api.nativapps.model.StudentModel;

// TODO: Auto-generated Javadoc
/**
 * The Interface StudentService.
 */
public interface StudentService {

	/**
	 * Adds the.
	 *
	 * @param studentModel the student model
	 * @return the student model
	 */
	public abstract StudentModel add(StudentModel studentModel);
	
	/**
	 * List all.
	 *
	 * @return the list
	 */
	public abstract List<StudentModel> listAll();
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the student
	 */
	public abstract Student findById(long id);
	
	/**
	 * Find model by id.
	 *
	 * @param id the id
	 * @return the student model
	 */
	public abstract StudentModel findModelById(long id);
	
	/**
	 * Removes the.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public abstract void remove(long id) throws Exception; 
	
}
