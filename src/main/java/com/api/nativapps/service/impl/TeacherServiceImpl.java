package com.api.nativapps.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.api.nativapps.component.TeacherConverter;
import com.api.nativapps.entity.Teacher;
import com.api.nativapps.model.TeacherModel;
import com.api.nativapps.repository.TeacherRepository;
import com.api.nativapps.service.TeacherService;

// TODO: Auto-generated Javadoc
/**
 * The Class TeacherServiceImpl.
 */
@Service("teacherServiceImpl")
public class TeacherServiceImpl implements TeacherService{

	/** The teacher repository. */
	@Autowired
	@Qualifier("teacherRepository")
	private TeacherRepository teacherRepository;

	/** The teacher converter. */
	@Autowired
	@Qualifier("teacherConverter")
	public TeacherConverter teacherConverter;
	
	/* (non-Javadoc)
	 * @see com.api.nativapps.service.TeacherService#add(com.api.nativapps.model.TeacherModel)
	 */
	@Override
	public TeacherModel add(TeacherModel studentModel) {
		Teacher teacher = teacherRepository.save(teacherConverter.convertModelToEntity(studentModel));
		return teacherConverter.convertEntityToModel(teacher);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.TeacherService#listAll()
	 */
	@Override
	public List<TeacherModel> listAll() {
		return teacherConverter.convertEntityListToModelList(teacherRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.TeacherService#findById(long)
	 */
	@Override
	public Teacher findById(long id) {
		return teacherRepository.findById(id);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.TeacherService#findModelById(long)
	 */
	@Override
	public TeacherModel findModelById(long id) {
		return teacherConverter.convertEntityToModel(findById(id));
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.TeacherService#remove(long)
	 */
	@Override
	public void remove(long id) throws Exception{
		Teacher teacher = findById(id);
		if (teacher != null) {
			teacherRepository.delete(teacher);
		}else{
			throw new Exception();
		}		
	}

}
