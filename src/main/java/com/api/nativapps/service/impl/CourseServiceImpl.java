package com.api.nativapps.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.api.nativapps.component.CourseConverter;
import com.api.nativapps.component.StudentConverter;
import com.api.nativapps.entity.Course;
import com.api.nativapps.entity.Course;
import com.api.nativapps.model.CourseModel;
import com.api.nativapps.repository.CourseRepository;
import com.api.nativapps.repository.StudentRepository;
import com.api.nativapps.service.CourseService;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseServiceImpl.
 */
@Service("courseServiceImpl")
public class CourseServiceImpl implements CourseService{

	/** The course repository. */
	@Autowired
	@Qualifier("courseRepository")
	private CourseRepository courseRepository;

	/** The course converter. */
	@Autowired
	@Qualifier("courseConverter")
	public CourseConverter courseConverter;
	
	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CourseService#add(com.api.nativapps.model.CourseModel)
	 */
	@Override
	public CourseModel add(CourseModel courseModel) {
		Course student = courseRepository.save(courseConverter.convertModelToEntity(courseModel));
		return courseConverter.convertEntityToModel(student);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CourseService#listAll()
	 */
	@Override
	public List<CourseModel> listAll() {
		return courseConverter.convertEntityListToModelList(courseRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CourseService#findByCode(java.lang.String)
	 */
	@Override
	public Course findByCode(String code) {
		return courseRepository.findByCode(code);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CourseService#findModelByCode(java.lang.String)
	 */
	@Override
	public CourseModel findModelByCode(String code) {
		// TODO Auto-generated method stub
		return courseConverter.convertEntityToModel(findByCode(code));
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CourseService#remove(java.lang.String)
	 */
	@Override
	public void remove(String code) throws Exception{
		Course student = findByCode(code);
		if (student != null) {
			courseRepository.delete(student);
		}else{
			throw new Exception();
		}
	}

}
