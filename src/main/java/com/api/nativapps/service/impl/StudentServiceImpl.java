package com.api.nativapps.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.api.nativapps.component.StudentConverter;
import com.api.nativapps.entity.Student;
import com.api.nativapps.model.StudentModel;
import com.api.nativapps.repository.StudentRepository;
import com.api.nativapps.service.StudentService;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentServiceImpl.
 */
@Service("studentServiceImpl")
public class StudentServiceImpl implements StudentService {

	/** The student repository. */
	@Autowired
	@Qualifier("studentRepository")
	private StudentRepository studentRepository;

	/** The student converter. */
	@Autowired
	@Qualifier("studentConverter")
	public StudentConverter studentConverter;

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.StudentService#add(com.api.nativapps.model.StudentModel)
	 */
	@Override
	public StudentModel add(StudentModel studentModel) {
		Student student = studentRepository.save(studentConverter.convertModelToEntity(studentModel));
		return studentConverter.convertEntityToModel(student);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.StudentService#listAll()
	 */
	@Override
	public List<StudentModel> listAll() {
		return studentConverter.convertEntityListToModelList(studentRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.StudentService#remove(long)
	 */
	@Override
	public void remove(long id) throws Exception{
		Student student = findById(id);
		if (student != null) {
			studentRepository.delete(student);
		}else{
			throw new Exception();
		}
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.StudentService#findById(long)
	 */
	@Override
	public Student findById(long id) {
		return studentRepository.findById(id);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.StudentService#findModelById(long)
	 */
	@Override
	public StudentModel findModelById(long id) {
		// TODO Auto-generated method stub
		return studentConverter.convertEntityToModel(findById(id));
	}

}
