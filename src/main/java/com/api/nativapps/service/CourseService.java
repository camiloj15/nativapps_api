package com.api.nativapps.service;

import java.util.List;

import com.api.nativapps.entity.Course;
import com.api.nativapps.entity.Student;
import com.api.nativapps.model.CourseModel;

// TODO: Auto-generated Javadoc
/**
 * The Interface CourseService.
 */
public interface CourseService {
	
	/**
	 * Adds the.
	 *
	 * @param courseModel the course model
	 * @return the course model
	 */
	public abstract CourseModel add(CourseModel courseModel);
	
	/**
	 * List all.
	 *
	 * @return the list
	 */
	public abstract List<CourseModel> listAll();
	
	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the course
	 */
	public abstract Course findByCode(String code);
	
	/**
	 * Find model by code.
	 *
	 * @param code the code
	 * @return the course model
	 */
	public abstract CourseModel findModelByCode(String code);
	
	/**
	 * Removes the.
	 *
	 * @param code the code
	 * @throws Exception the exception
	 */
	public abstract void remove(String code) throws Exception; 

}
