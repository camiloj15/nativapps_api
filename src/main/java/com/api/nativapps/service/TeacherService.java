package com.api.nativapps.service;

import java.util.List;

import com.api.nativapps.entity.Teacher;
import com.api.nativapps.model.TeacherModel;

// TODO: Auto-generated Javadoc
/**
 * The Interface TeacherService.
 */
public interface TeacherService {

	/**
	 * Adds the.
	 *
	 * @param studentModel the student model
	 * @return the teacher model
	 */
	public abstract TeacherModel add(TeacherModel studentModel);
	
	/**
	 * List all.
	 *
	 * @return the list
	 */
	public abstract List<TeacherModel> listAll();
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the teacher
	 */
	public abstract Teacher findById(long id);
	
	/**
	 * Find model by id.
	 *
	 * @param id the id
	 * @return the teacher model
	 */
	public abstract TeacherModel findModelById(long id);
	
	/**
	 * Removes the.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public abstract void remove(long id) throws Exception; 
	
}
