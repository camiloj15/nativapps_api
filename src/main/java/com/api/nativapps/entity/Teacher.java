package com.api.nativapps.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class Teacher.
 */
@Entity
@Table(name = "teachers")
public class Teacher {

	/** The id. */
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id", unique=true, nullable=false)
	private long id;
	
	/** The courses. */
	@OneToMany(fetch=FetchType.EAGER, mappedBy="teacher")
	private Set<Course> courses = new HashSet<Course>();
	
	/** The identification. */
	@Column(name="identification", nullable=false)
	private String identification;
	
	/** The name. */
	@Column(name="name", nullable=false)
	private String name;
	
	/** The last name. */
	@Column(name="lastName", nullable=false)
	private String lastName;
	
	/** The genre. */
	@Column(name="genre", nullable=false)
	private String genre;

	/**
	 * Instantiates a new teacher.
	 *
	 * @param id the id
	 * @param courses the courses
	 * @param identification the identification
	 * @param name the name
	 * @param lastName the last name
	 * @param genre the genre
	 */
	public Teacher(long id, Set<Course> courses, String identification, String name, String lastName, String genre) {
		super();
		this.id = id;
		this.courses = courses;
		this.identification = identification;
		this.name = name;
		this.lastName = lastName;
		this.genre = genre;
	}

	/**
	 * Instantiates a new teacher.
	 */
	public Teacher() {
		
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the courses.
	 *
	 * @return the courses
	 */
	public Set<Course> getCourses() {
		return courses;
	}

	/**
	 * Sets the courses.
	 *
	 * @param courses the new courses
	 */
	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	/**
	 * Gets the identification.
	 *
	 * @return the identification
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * Sets the identification.
	 *
	 * @param identification the new identification
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the genre.
	 *
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * Sets the genre.
	 *
	 * @param genre the new genre
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Teacher [id=" + id + ", courses=" + courses + ", identification=" + identification + ", name=" + name
				+ ", lastName=" + lastName + ", genre=" + genre + "]";
	}
	
}
