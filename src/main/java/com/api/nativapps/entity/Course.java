package com.api.nativapps.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

// TODO: Auto-generated Javadoc
/**
 * The Class Course.
 */
@Entity
@Table(name = "courses")
public class Course {
	
	/** The code. */
	@Id
	@Column(name="code", unique=true, nullable=false)
	private String code;
	
	/** The students course. */
	@ManyToMany
    @JoinTable(name="students_course",
        joinColumns=
            @JoinColumn(name="course_id", referencedColumnName="code"),
        inverseJoinColumns=
            @JoinColumn(name="student_id", referencedColumnName="id")
        )
	private Set<Student> students_course = new HashSet<Student>();
	
	/** The observation. */
	@Column(name="observation")
	private String observation;
	
	/** The name. */
	@Column(name="name", nullable=false)
	private String name;
	
	/** The teacher. */
	@ManyToOne(fetch= FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	@JoinColumn(name="teacher_id")
	private Teacher teacher;

	/**
	 * Instantiates a new course.
	 *
	 * @param code the code
	 * @param students_course the students course
	 * @param observation the observation
	 * @param name the name
	 * @param teacher the teacher
	 */
	public Course(String code, Set<Student> students_course, String observation, String name, Teacher teacher) {
		super();
		this.code = code;
		this.students_course = students_course;
		this.observation = observation;
		this.name = name;
		this.teacher = teacher;
	}

	/**
	 * Instantiates a new course.
	 */
	public Course() {
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the students course.
	 *
	 * @return the students course
	 */
	public Set<Student> getStudents_course() {
		return students_course;
	}

	/**
	 * Sets the students course.
	 *
	 * @param students_course the new students course
	 */
	public void setStudents_course(Set<Student> students_course) {
		this.students_course = students_course;
	}

	/**
	 * Gets the observation.
	 *
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * Sets the observation.
	 *
	 * @param observation the new observation
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the teacher.
	 *
	 * @return the teacher
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * Sets the teacher.
	 *
	 * @param teacher the new teacher
	 */
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Course [code=" + code + ", students_course=" + students_course + ", observation=" + observation
				+ ", name=" + name + ", teacher=" + teacher + "]";
	}
	
	

}
