package com.api.nativapps.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.nativapps.entity.Student;

// TODO: Auto-generated Javadoc
/**
 * The Interface StudentRepository.
 */
@Repository("studentRepository")
public interface StudentRepository extends JpaRepository<Student, Serializable>{

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the student
	 */
	public abstract Student findById(long id);

}
