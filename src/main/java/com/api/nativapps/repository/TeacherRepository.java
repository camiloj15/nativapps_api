package com.api.nativapps.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.nativapps.entity.Student;
import com.api.nativapps.entity.Teacher;

// TODO: Auto-generated Javadoc
/**
 * The Interface TeacherRepository.
 */
@Repository("teacherRepository")
public interface TeacherRepository extends JpaRepository<Teacher, Serializable>{

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the teacher
	 */
	public abstract Teacher findById(long id);

}
