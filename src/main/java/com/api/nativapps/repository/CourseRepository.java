package com.api.nativapps.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.nativapps.entity.Course;
import com.api.nativapps.entity.Student;

// TODO: Auto-generated Javadoc
/**
 * The Interface CourseRepository.
 */
@Repository("courseRepository")
public interface CourseRepository extends JpaRepository<Course, Serializable>{

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the course
	 */
	public abstract Course findByCode(String code);

}
