package com.api.nativapps.constants;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilsConstants.
 */
public class UtilsConstants {
	
	/** The Constant CONTACT_FORM. */
	//VIEWS
	public final static String CONTACT_FORM = "contactform";
	
	/** The Constant LOGIN_VIEW. */
	public final static String LOGIN_VIEW = "login";
	
	/** The Constant CONTACTS_VIEW. */
	public final static String CONTACTS_VIEW = "contacts";
	
	/** The Constant LOGINCHECK. */
	public final static String LOGINCHECK = "loginsucess";
	
	/** The Constant STUDENT_CREATED. */
	//STUDENTS CONTROLLER
	public final static String STUDENT_CREATED = "Estudiante agregado exitosamente";
	
	/** The Constant STUDENT_UPDATED. */
	public final static String STUDENT_UPDATED = "Estudiante actualizado exitosamente";
	
	/** The Constant STUDENT_DELETED. */
	public final static String STUDENT_DELETED = "Estudiante eliminado exitosamente";
	
	/** The Constant STUDENT_ERROR_DELETED. */
	public final static String STUDENT_ERROR_DELETED = "Estudiante no encontrado";
	
	/** The Constant STUDENT_ERROR_CREATED. */
	public final static String STUDENT_ERROR_CREATED = "Error al crear el estudiante, favor completar los campos";

	/** The Constant COURSE_CREATED. */
	//TEACHER CONTROLLER
	public final static String COURSE_CREATED = "Curso agregado exitosamente";
	
	/** The Constant COURSE_UPDATED. */
	public final static String COURSE_UPDATED = "Curso actualizado exitosamente";
	
	/** The Constant COURSE_DELETED. */
	public final static String COURSE_DELETED = "Curso eliminado exitosamente";
	
	/** The Constant COURSE_ERROR_DELETED. */
	public final static String COURSE_ERROR_DELETED = "Curso no encontrado";
	
	/** The Constant COURSE_ERROR_CREATED. */
	public final static String COURSE_ERROR_CREATED = "Error al crear el curso, favor completar los campos";
	
	/** The Constant TEACHER_CREATED. */
	//TEACHER CONTROLLER
	public final static String TEACHER_CREATED = "Docente agregado exitosamente";
	
	/** The Constant TEACHER_UPDATED. */
	public final static String TEACHER_UPDATED = "Docente actualizado exitosamente";
	
	/** The Constant TEACHER_DELETED. */
	public final static String TEACHER_DELETED = "Docente eliminado exitosamente";
	
	/** The Constant TEACHER_ERROR_DELETED. */
	public final static String TEACHER_ERROR_DELETED = "Docente no encontrado";
	
	/** The Constant TEACHER_ERROR_CREATED. */
	public final static String TEACHER_ERROR_CREATED = "Error al crear el cocente, favor completar los campos";
}
