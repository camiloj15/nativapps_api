package com.api.nativapps.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.api.nativapps.entity.Course;
import com.api.nativapps.entity.User;

// TODO: Auto-generated Javadoc
/**
 * The Class StudentModel.
 */
public class StudentModel {

	/** The id. */
	private long id;
	
	/** The user. */
	private User user;
	
	/** The courses. */
	private Set<Course> courses = new HashSet<Course>();
	
	/** The identification. */
	private String identification;
	
	/** The name. */
	private String name;
	
	/** The last name. */
	private String lastName;
	
	/** The genre. */
	private String genre;

	/**
	 * Instantiates a new student model.
	 *
	 * @param id the id
	 * @param user the user
	 * @param identification the identification
	 * @param name the name
	 * @param lastName the last name
	 * @param genre the genre
	 */
	public StudentModel(long id, User user, String identification, String name, String lastName, String genre) {
		super();
		this.id = id;
		this.user = user;
		this.identification = identification;
		this.name = name;
		this.lastName = lastName;
		this.genre = genre;
	}

	/**
	 * Instantiates a new student model.
	 */
	public StudentModel() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Gets the identification.
	 *
	 * @return the identification
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * Sets the identification.
	 *
	 * @param identification the new identification
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the genre.
	 *
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * Sets the genre.
	 *
	 * @param genre the new genre
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StudentModel [id=" + id + ", user=" + user + ", identification=" + identification + ", name=" + name
				+ ", lastName=" + lastName + ", genre=" + genre + "]";
	}
	

}
